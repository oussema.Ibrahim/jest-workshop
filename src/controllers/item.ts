import { RequestHandler } from "express";
import asyncHandler from "express-async-handler";
import { Types } from "mongoose";
import { ItemModel } from "../db/model/Item";
import { IItem } from "../types";

export const addItem: RequestHandler = asyncHandler(async (req, res) => {
  const doc = await ItemModel.create(req.body as IItem);

  res.status(201).json({
    status: "success",
    data: doc,
  });
});

export const deleteItem: RequestHandler = asyncHandler(
  async (req, res, next) => {
    const item = await ItemModel.findByIdAndDelete(
      new Types.ObjectId(req.params.id)
    );

    if (!item) {
      return next(
        res.status(404).json({
          status: "failed",
        })
      );
    }

    res.status(204).json({
      status: "success",
      data: null,
    });
  }
);

export const getItem: RequestHandler = asyncHandler(async (req, res, next) => {
  const item = await ItemModel.findById(new Types.ObjectId(req.params.id));

  if (!item) {
    return next(res.status(404).json({ status: "failed" }));
  }

  res.status(200).json({
    status: "success",
    data: item,
  });
});
export const updateItem: RequestHandler = asyncHandler(
  async (req, res, next) => {
    const item = await ItemModel.findByIdAndUpdate(
      new Types.ObjectId(req.params.id),
      req.body as IItem,
      {
        new: true,
      }
    );

    if (!item) {
      return next(res.status(404).json({ status: "failed" }));
    }

    res.status(200).json({
      status: "success",
      data: item,
    });
  }
);

export const getItems: RequestHandler = asyncHandler(async (req, res, next) => {
  const item = await ItemModel.find();

  res.status(200).send({
    message: "success",
    data: item,
  });
});

import express from "express";
import bodyParser from "body-parser";
import { connectDb } from "./db/conectDb";
import config from "./config";
import itemRoutes from "./router/items";

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/api/items", itemRoutes);

connectDb();

app.listen(config.port, () => {
  console.log(`Server started on port ${config.port}`);
});
 
export default app
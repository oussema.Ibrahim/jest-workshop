import { model, QueryOptions, Schema } from "mongoose";
import { IItem } from "../../types";

const DOCUMENT_NAME = "Item";
const COLLECTION_NAME = "items";

const schema = new Schema(
  {
    name: {
      type: String,
      required: [true, "You should provide a name"],
    },
    description: {
      type: String,
      required: [true, "You should provide a name"],
    },
    price: {
      type: Number,
      required: [true, "You should provide a price"],
    },
  },
  { versionKey: false, timestamps: true }
);

schema.pre(/^find/, function (this: QueryOptions, next) {
  this.select("-createdAt -updatedAt");

  next();
});
export const ItemModel = model<IItem>(
  DOCUMENT_NAME,
  schema,
  COLLECTION_NAME
);

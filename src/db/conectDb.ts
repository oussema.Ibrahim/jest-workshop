import mongoose from "mongoose";
import config from "../config";

export const connectDb = async () => {
  try {
    await mongoose.connect(config.mongodbUrl);
    console.log("DB connected successfully !");
  } catch (e) {
    console.log(e);
  }
};

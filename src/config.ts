import dotenv from "dotenv";
dotenv.config();
const { NODE_ENV, PORT, DB_URL } = process.env;


const config = {
  env: NODE_ENV ?? "development",
  port: PORT ? parseInt(PORT, 10) : 3030,
  mongodbUrl: DB_URL ?? "mongodb://localhost/jest-workshop",
};

export default config;

import express from "express";
import {
  addItem,
  deleteItem,
  getItem,
  getItems,
  updateItem,
} from "../controllers/item";

const router = express.Router();

router.post("/", addItem);
router.get("/", getItems);
router.get("/:id", getItem);
router.delete("/:id", deleteItem);
router.patch("/:id", updateItem);

export default router;

import  supertest from 'supertest';
import app from '../src/app';

const request = supertest(app);

describe('Items API', () => {
  let itemId=''
 

  it('should create a new Item', async () => {
    const response = await request.post('/api/items').send({
      name: 'milk',
      description: 'White, creamy',
      price: 18,
    });
    expect(response.status).toBe(201);
    expect(response.body.data.name).toBe('milk');
    expect(response.body.data.description).toBe('White, creamy');
    itemId = response.body.data._id;
  });

  it('should get a list of items', async () => {
    const response = await request.get('/api/items');

    expect(response.status).toBe(200);
    expect(Array.isArray(response.body.data)).toBe(true);
    expect(response.body.data.length).toBeGreaterThan(0);
  });

  it('should get an item by ID', async () => {
    const response = await request.get(`/api/items/${itemId}`);
    expect(response.status).toBe(200);
    expect(response.body.data._id).toBe(itemId);
    expect(response.body.data.name).toBe('milk');
    expect(response.body.data.description).toBe('White, creamy');
  });

  it('should update an Item', async () => {
    const response = await request.patch(`/api/items/${itemId}`).send({
      name: 'honey',
    });
    expect(response.status).toBe(200);
    expect(response.body.data._id).toBe(itemId);
    expect(response.body.data.name).toBe('honey');
    expect(response.body.data.description).toBe('White, creamy');
  });

  it('should delete an item', async () => {
    const response = await request.delete(`/api/items/${itemId}`);
    expect(response.status).toBe(204);
    const checkItemResponse = await request.get(`/api/items/${itemId}`);
    expect(checkItemResponse.status).toBe(404);
  });
});
